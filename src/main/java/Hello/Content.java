package Hello;

import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class Content {
    private String nome = "Ighor Eduardo Vieira Peres";
    private int numero = new Random().nextInt(101);
    private String horas = getTime();
    //private LocalTime horaRaiz = java.time.LocalTime.now();

    private String getTime(){
        Calendar data = Calendar.getInstance();
        int hora = data.get(Calendar.HOUR_OF_DAY);
        int min = data.get(Calendar.MINUTE);
        String horas = hora + ":" + min;
        return horas;
    }

    public String getNome(){
        return nome;
    }
    public int getNumero(){
        return numero;
    }

    public String getHoras(){
        return horas;
    }

    /*public LocalTime getHoraRaiz(){
        //return horaRaiz;
    }
    */

}
